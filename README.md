# WiFi Checker

## What is it?
This is just a shell script that reconnects WiFi if there is a disconnect for most common reasons. Only works on Linux, is especially useful for Beagle Bones and Raspberry Pis.

## How To Install
* run `wget https://bitbucket.org/troyblank/wifi-checker/raw/master/networkChecker.sh -O /usr/local/sbin/networkChecker.sh`.
* change any variables you desire in networkChecker.sh.
* run `chmod 755 /usr/local/bin/networkCheck.sh`.
* run `sudo crontab -e` and enter the following to run the check every 5 minutes, plus keep your WiFi awake with an external ping.


    \*/3 \* \* \* \* ping -c 1 google.com >> /dev/null 2>&1  
    \*/5 \* \* \* \* /usr/local/bin/networkCheck.sh >> /dev/null 2>&1  